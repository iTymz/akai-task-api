<?php
require 'vendor/autoload.php';

// GET /repos/:owner/:repo/issues
const API_ENDPOINT = 'https://api.github.com/repos/akai-org/trios/issues?per_page=5';

$client = new GuzzleHttp\Client();
$res = $client->request('GET', API_ENDPOINT);
$data = $res->getBody();

$jsonData = json_decode($data, true);

?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>task-php</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<div class="row">
    <div class="list-group col-md-4 col-md-offset-4">
        <ul>
            <li class="list-group-item active">
                Last issues
            </li>
            <?php
            foreach ($jsonData as $issue) {
                echo sprintf('<a href="%s" class="list-group-item">%s</a>', $issue['html_url'], $issue['title']);
            }
            ?>
        </ul>
    </div>
</div>
</body>
</html>
